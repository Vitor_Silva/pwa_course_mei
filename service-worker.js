var cacheName = 'v1';

var  staticAssets = [
    "/css/main.css",
    "/img/icon-192x192.png",
    "/img/icon-512x512.png",
    "/img/header.png",
    "/js/main.js",
    "/",
    "/index.html"
];

self.addEventListener('install' , async event => {
    const cache = await caches.open(cacheName);
    await cache.addAll(staticAssets);
});

self.addEventListener('fetch', async event =>{
    const cache = await caches.open(cacheName);
    event.respondWith(
        cache.match(event.request)
    );
});